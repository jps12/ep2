/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class Veiculo {
    protected int quantidade;
    protected int cargaMaxima;
    protected int velocidadeMedia;
    
    public int getQuantidade(){
        return quantidade;
    }
    public double CalculaTempo(double distancia){
        return distancia/velocidadeMedia;
    }
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }
    public boolean IsPossible(int carga, double tempo, double distancia){
        if (carga>cargaMaxima||CalculaTempo(distancia)>tempo||quantidade==0){
            return false;
        }
        return true;
    }
}
