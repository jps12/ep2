/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class Moto extends VeiculoFlex {
    
    
    public Moto(){
        super();
        rendimentoAlcool = 43;
        rendimentoGasolina = 50;
        cargaMaxima = 50;
        velocidadeMedia = 110; 
        reducaoAlcool = 0.3;
        reducaoGasolina = 0.4;
    }
}
