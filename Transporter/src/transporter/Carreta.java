/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class Carreta extends VeiculoDisel{
    
    
    public Carreta(){
        super();
        rendimentoDisel = 8;
        cargaMaxima = 30000;
        velocidadeMedia = 60;
        reducaoRendimento = 0.001;
    }
}
