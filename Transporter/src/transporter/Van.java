/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class Van extends VeiculoDisel {
    
    
    public Van(){
        super();
        rendimentoDisel = 10;
        cargaMaxima = 3500;
        velocidadeMedia = 80;
        reducaoRendimento = 0.001;
    }
}
