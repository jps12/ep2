package transporter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joao
 */
public class Carro  extends VeiculoFlex {
    
    
    public Carro(){
        super();
        rendimentoAlcool = 12;
        rendimentoGasolina = 14;
        cargaMaxima = 360;
        velocidadeMedia = 100; 
        reducaoAlcool = 0.0231;
        reducaoGasolina = 0.025;
    }
}
