/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class VeiculoDisel extends Veiculo{
    protected double precoDisel;
    protected double reducaoRendimento;
    protected double rendimentoDisel;
    
    public VeiculoDisel(){
        precoDisel = 3.869;
    }
    
    public double CalculaCusto(double distancia, double carga){
        return (distancia/(rendimentoDisel - carga * reducaoRendimento)) * precoDisel;
    }
    
}
