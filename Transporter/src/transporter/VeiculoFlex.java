/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporter;

/**
 *
 * @author joao
 */
public class VeiculoFlex extends Veiculo{
    protected double precoAlcool;
    protected double precoGasolina;
    protected double reducaoAlcool;
    protected double reducaoGasolina;
    protected double rendimentoAlcool;
    protected double rendimentoGasolina;
    
    public VeiculoFlex(){
        precoAlcool = 3.499;
        precoGasolina = 4.449;
    }
    public double CalculaCusto(double distancia, double carga){
        double custoAlcool, custoGasolina;
        custoAlcool = (distancia/(rendimentoAlcool - carga * reducaoAlcool)) * precoAlcool;
        custoGasolina = (distancia/(rendimentoGasolina - carga * reducaoGasolina)) * precoGasolina;
        return Math.min(custoGasolina, custoAlcool);
    }
}
