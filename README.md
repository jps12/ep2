# EP2   2019_1

##Usabilidade:
Ao executar o programa o usuário terá opção de:
    * Visualizar os veículos disponíveis.
    * Alterar a quantidade de veículos disponíveis.
    * Alterar a margem de lucro.
    * Calcular o preço das entregas mais rápida, mais barata e com o maior custo benefício.

### Visulizar os veículos disponíveis:
    Ao selecionar essa opção será aberta ao usuário uma nova janela mostrando as quantidades atuais de cada veículo. Ao fechar a janela, o programa retornará à janela principal.

### Alterar a quantidade dos veículos:
    Ao selecionar essa opção será aberta uma janela com espaços para preencher a nova quantidade de cada veículo e também mostrará a quantidade atual de cada veículo. Ao digitar o novo valor é necessário pressionar enter para grava o novo valor. Feito isso a quantidade atual que está sendo mostrada será atualizada. Ao fechar a janela o programa retornará a janela principal.

### Altrar a margem de lucro:
    Será aberta uma nova janela em que é mostrado a margem de lucro atual e um espaço para alterar a margem de lucro. É necessário pressionar enter para o programa receber o valor e alterar a margem. Feito isso o valor atual mostrado na tela irá atualizar automaticamente. Ao fechar a tela o programa retorna a janela principal.

### Fazer entrega:
    Abrirá uma tela para o usuário para inserir os dados da entrega. Ao inserir todos os dados, pressionando enter em cada um e apertando "Calcular frete", o programa exibirá o frete mais rápido disponível, o frete mais barato e o de menor custo benefício. Irá aparecer botões para selecionar o veiculo desejado - apenas os veículos disponíveis aparecerão. ao selecionar alguma opção, aparecerá uma mensagem confirmando a entrega e o programa retornará a janela principal.

##Calculos: 

###Custo-benefício:
    Para o calculo do custo beneficio foi usado a formula Lucro/TempoEntrega.

### Custo do combustível:
    Caso seja possível nos veículos a disel o programa retornará o calculo: (distanciaEntrega/(rendimentoDisel - carga * reducaoDisel)). Ja nos veiculos Flex, o programa irá calcular os custos para os dois cobustíveis e irá retornar o de menor valor.